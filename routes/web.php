<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('profile', 'ProfileController')->only(['edit', 'update', 'destroy'])->middleware('auth');
Route::get('profile/pause/{profile}', 'ProfileController@pause')->name('profile.pause')->middleware('auth');
Route::put('profile/continue/{profile}', 'ProfileController@continue')->name('profile.continue')->middleware('auth');
Route::resource('add-donor', 'AddDonorController')->only(['create','store', 'edit', 'update', 'destroy'])->middleware(['admin', 'auth']);
