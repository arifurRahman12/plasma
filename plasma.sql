-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 26, 2020 at 03:55 PM
-- Server version: 10.3.22-MariaDB
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `genuinefa_plasma`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(115, '2014_10_12_000000_create_users_table', 1),
(116, '2014_10_12_100000_create_password_resets_table', 1),
(117, '2019_08_19_000000_create_failed_jobs_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role` enum('donor','admin') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'donor',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `blood` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gotWell` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role`, `name`, `phone`, `password`, `blood`, `gotWell`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Arifur Rahman', '01675756193', '$2y$10$XyRQwhddIXROMt/62KAJZO/QQm/.NK/X3pLnrWsp7pUTTnTWqMyCW', NULL, '2020-05-23 03:47:35', NULL, '2020-05-23 03:47:35', '2020-05-23 03:47:35'),
(15, 'donor', 'Hadiul Islam Shojib', '01932426840', '$2y$10$pwrweTa0VVtMu45T/dMoC.voJu3magnnG.OX2ehZppP8caLuJ3lUK', NULL, '2020-05-23 09:02:56', NULL, '2020-05-23 09:02:56', '2020-05-23 09:02:56'),
(16, 'donor', 'Tanveer ahmed', '01711904522', '$2y$10$WCF2iD5YMEiImv/K4hv/dOME/MW0VyZaJ6X0wnGMgm3MKNmhpaVui', 'B+', '2020-05-23 09:07:14', NULL, '2020-05-23 09:07:14', '2020-05-23 09:07:14'),
(17, 'donor', 'Md jakir husan', '01756595052', '$2y$10$mb8KYrJg88rNdIZi5OKaaOmROq5fbBHEc5vGj8ubtcd5..aMOoEmW', 'O+', '2020-05-23 09:16:40', NULL, '2020-05-23 09:16:40', '2020-05-23 09:16:40'),
(18, 'donor', 'Md Dauwd', '01681722328', '$2y$10$GbIxeQ6X3Py9H.YM5ISweeII273Xq8Mxta7Cf.SSLHS0Lo6uQsmyW', NULL, '2020-05-22 18:00:00', NULL, '2020-05-23 09:53:05', '2020-05-23 09:56:29'),
(19, 'donor', 'Nahid', '01722966499', '$2y$10$S3GSyBCLr2ZDhiX.PkzmIOpGtmJHktRDacZz/nnEtXxdauzHw88he', NULL, '2020-05-23 10:13:59', NULL, '2020-05-23 10:13:59', '2020-05-23 10:13:59'),
(20, 'donor', 'Anik', '01680728542', '$2y$10$dUtzv9ezKfyT0JIhHiQ9o.qwnNYQFIauHm4C0xjZuYmhluu2p/LB6', NULL, '2020-05-23 10:43:58', NULL, '2020-05-23 10:43:58', '2020-05-23 10:43:58'),
(21, 'donor', 'Sarfaraz', '01680086516', '$2y$10$lyXQ7DFkzn2hiok5HyWSyeRpfS17X28ejsX0kL2ypLTMMiFhD5F7S', NULL, '2020-05-23 14:41:54', NULL, '2020-05-23 14:41:54', '2020-05-23 14:41:54'),
(22, 'donor', 'Misba', '01647395141', '$2y$10$TFP/JW8U7CgUCT0k6flEAOnmzMJPdsuEgcmb9Cz92shGaXUtpGdlO', NULL, '2020-05-24 08:45:31', NULL, '2020-05-24 08:45:31', '2020-05-24 08:45:31'),
(23, 'donor', 'new', '12233322231', '$2y$10$K2Jpc8e3M1irviN5KzN32.vBLifBJU8gdvzRl0XKXbMHXcl6xC4Ui', NULL, '2020-05-24 08:59:41', NULL, '2020-05-24 08:59:41', '2020-05-24 10:15:41'),
(24, 'donor', 'Sharifur Rahman', '01630151515', '$2y$10$3h4S1LbzJ1FnQk7P1gjHZ.Xu9.SfRCnqgBQFGeDLz1JmLGw.HceJW', NULL, '2020-05-25 08:16:32', NULL, '2020-05-25 08:16:33', '2020-05-25 08:16:33');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=118;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
