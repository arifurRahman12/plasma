<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'role' => 'admin',
            'name' => 'Arifur Rahman',
            'phone' => '01675756193',
            'password' => Hash::make('password'),
            'gotWell' => now(),
        ]);
    }
}
