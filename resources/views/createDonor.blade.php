@extends('layouts.master')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">নতুন দাতা এড করুন</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                            <li class="breadcrumb-item active">add-donor</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">

                <!-- Main row -->
                <div class="row">
                    <!-- Left col -->
                    <section class="col-lg-12">
                        <!-- Custom tabs (Charts with tabs)-->
                        <div class="card">
                            <div class="card-header bg-success font-weight-bold">

                                এড করুন
                            </div><!-- /.card-header -->
                            <div class="card-body">
                                <form action="{{ route('add-donor.store') }}" method="POST">
                                    @csrf
                                    <div class="form-row">
                                        <div class="form-group col-md-4">
                                            <label for="name">নাম <span class="text-danger font-weight-bold h4"> *</span></label>
                                            <input name="name" type="text" class="form-control" id="name" value="{{ old('name') }}" required>
                                            @error('name')
                                            <div class="text-danger">{{ $message }}</div>
                                            @enderror
                                        </div>

                                        <div class="form-group col-md-4">
                                            <label for="phone">ফোন নাম্বার <span class="text-danger font-weight-bold h4"> *</span></label>
                                            <input name="phone" type="text" class="form-control" id="phone" value="{{ old('phone') }}" required>
                                            @error('phone')
                                            <div class="text-danger">{{ $message }}</div>
                                            @enderror
                                        </div>

                                        <div class="form-group col-md-4">
                                            <label for="password">দাতার পাসওয়ার্ড</label>
                                            <input name="password" type="password" class="form-control" id="password" required>
                                            @error('password')
                                            <div class="text-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="password-confirm">দাতার পাসওয়ার্ডটি পুনরায় দিন</label>
                                            <input name="password_confirmation" type="password" class="form-control" id="password-confirm" required>
                                        </div>

                                        <div class="form-group col-md-4">
                                            <label for="blood">রক্তের গ্রুপ<span class="text-danger"> (শুধুমাত্র প্লাজমা দাতা হলে)</span></label>
                                            <select class="form-control" name="blood" id="blood">
                                                <option value="">Select</option>
                                                <option value="O+">O+</option>
                                                <option value="O-">O-</option>
                                                <option value="A+">A+</option>
                                                <option value="A-">A-</option>
                                                <option value="B+">B+</option>
                                                <option value="B-">B-</option>
                                                <option value="AB+">AB+</option>
                                                <option value="AB-">AB-</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="role">দায়িত্ব বাছাই করুন</label>
                                            <select class="form-control" name="role" id="role">
                                                <option value="">Select</option>
                                                <option value="user">প্লাজমা দাতা</option>
                                                <option value="admin">এডমিন</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-4 mt-4 pt-2">
                                            <input type="submit" value="এড করুন" class="btn btn-primary">
                                        </div>
                                    </div>
                                </form>
                                </form>

                            </div>
                        </div>
                    </section>
                    <form action="" method="POST" id="deleteProfileForm">
                    @method('DELETE')
                    @csrf
                    <!-- Modal -->
                        <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="deleteModalLabel">প্রোফাইল ডিলিট</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        আপনি কি আপনার প্রোফাইল পুরোপুরি ডিলিট করতে চান ?
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">না</button>
                                        <button type="submit" class="btn btn-danger">হ্যা, ডিলিট</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- Modal -->
                    <form action="" method="POST" id="continueProfileForm">
                        @method('PUT')
                        @csrf
                        <div class="modal fade" id="continueModal" tabindex="-1" role="dialog" aria-labelledby="continueModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="continueModalLabel">প্রোফাইল পুনরায় তালিকাভুক্ত করুন</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <label for="blood">রক্তের গ্রুপ <span class="text-danger">(প্লাজমা দাতা হলে)</span></label>
                                            <select class="form-control" id="blood" name="blood" required>
                                                <option value="">Select</option>
                                                <option value="O+">O+</option>
                                                <option value="O-">O-</option>
                                                <option value="A+">A+</option>
                                                <option value="A-">A-</option>
                                                <option value="B+">B+</option>
                                                <option value="B-">B-</option>
                                                <option value="AB+">AB+</option>
                                                <option value="AB-">AB-</option>
                                            </select>
                                            @error('blood')
                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">বাতিল</button>
                                        <button type="submit" class="btn btn-success">প্রোফাইল চালু করুন</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('css')
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('') }}/plugins/fontawesome-free/css/all.min.css">
    <link rel="stylesheet" href="{{ asset('') }}/dist/css/adminlte.min.css">
    {{--    <!-- overlayScrollbars -->--}}
    {{--    <link rel="stylesheet" href="{{ asset('') }}/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">--}}
    {{--    <!--  Google Font: Source Sans Pro -->--}}
    {{--    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">--}}
    {{--    Date picker--}}
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
@endsection

@section('script')
    <!-- jQuery -->
    <script src="{{ asset('') }}/plugins/jquery/jquery.min.js"></script>
    {{--    <!-- jQuery UI 1.11.4 -->--}}
    {{--    <script src="{{ asset('') }}/plugins/jquery-ui/jquery-ui.min.js"></script>--}}
    {{--    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->--}}
    {{--    <script>--}}
    {{--        $.widget.bridge('uibutton', $.ui.button)--}}
    {{--    </script>--}}
    {{--    --}}{{--    Popper JS--}}
    {{--    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>--}}
    <!-- Bootstrap 4 -->
    <script src="{{ asset('') }}/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- AdminLTE App -->
    <script src="{{ asset('') }}/dist/js/adminlte.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    <script>
        flatpickr('#gotWell');
    </script>
    <script>
        function handleDelete($id) {
            var form = document.getElementById('deleteProfileForm')
            form.action = '/profile/' + $id
            $('#deleteModal').modal('show')
        }
    </script>
    <script>
        function handleContinue($id) {
            var form = document.getElementById('continueProfileForm')
            form.action = '/profile/continue/' + $id
            $('#continueModal').modal('show')
        }
    </script>

@endsection

