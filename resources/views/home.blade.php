@extends('layouts.master')

@section('content')
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h5 class="m-0 text-success">রক্তের গ্রুপ দিয়ে সার্চ করুন</h5>
                        </div><!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                                <li class="breadcrumb-item active">User</li>
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

        <!-- Main content -->
            <section class="content">
                <div class="container-fluid">
                    @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif
                        @if (session('warning'))
                        <div class="alert alert-warning">
                            {{ session('warning') }}
                        </div>
                    @endif
                    <!-- Main row -->
                    <div class="row">
                        <!-- Left col -->
                        <section class="col-lg-12">
                            <!-- Custom tabs (Charts with tabs)-->
                            <div class="card">
                                <div class="card-header bg-dark font-weight-bold">
                                    দাতাদের তালিকা
                                    @auth()
                                        @if(Auth::user()->role === 'admin')
                                            <a class="btn btn-success btn-sm float-right" href="{{ route('add-donor.create') }}">পরিচিত প্লাজমা দাতা এড করুন</a>
                                        @else
                                        @endif
                                    @else

                                    @endauth

                                </div><!-- /.card-header -->
                                <div class="card-body">
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>আইডি</th>
                                            <th>নাম</th>
                                            <th>ফোন</th>
                                            <th>রক্তের গ্রুপ</th>
                                            <th>সুস্থ হওয়ার তারিখ</th>
                                            @auth()
                                                @if(Auth::user()->role === 'admin')
                                                    <th>একশন</th>
                                                @else

                                                @endif
                                            @else

                                            @endauth
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($users as $user)
                                            <tr>
                                                <td>{{ $user->id }}</td>
                                                <td>{{ $user->name }}</td>
                                                <td>{{ $user->phone }}</td>
                                                <td>{{ $user->blood }}</td>
                                                <td>{{ date('d-m-Y', strtotime($user->gotWell)) }}</td>
                                                @auth()
                                                    @if(Auth::user()->role == 'admin')
                                                        <td>
                                                            <a title="edit" class="btn btn-primary btn-sm ml-3" href="{{ route('add-donor.edit', $user->id) }}"><i class="fas fa-user-edit"></i></a>
                                                            <button title="delete" type="submit" class="btn btn-danger btn-sm ml-5" onclick="handleDelete({{ $user->id }})"><i class="fas fa-trash"></i></button>
                                                        </td>
                                                    @else

                                                    @endif

                                                @else

                                                @endauth
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div><!-- /.card-body -->
                                <form action="" method="POST" id="deleteProfileForm">
                                @method('DELETE')
                                @csrf
                                <!-- Modal -->
                                    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="deleteModalLabel">প্রোফাইল ডিলিট</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    আপনি কি এই প্রোফাইলটি পুরোপুরি ডিলিট করতে চান ?
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">না</button>
                                                    <button type="submit" class="btn btn-danger">হ্যা, ডিলিট</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <!-- Modal -->
                                <form action="" method="POST" id="continueProfileForm">
                                    @method('PUT')
                                    @csrf
                                    <div class="modal fade" id="continueModal" tabindex="-1" role="dialog" aria-labelledby="continueModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="continueModalLabel">প্রোফাইল পুনরায় তালিকাভুক্ত করুন</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="form-group">
                                                        <label for="blood">রক্তের গ্রুপ <span class="text-danger">(প্লাজমা দাতা হলে)</span></label>
                                                        <select class="form-control" id="blood" name="blood" required>
                                                            <option value="">Select</option>
                                                            <option value="O+">O+</option>
                                                            <option value="O-">O-</option>
                                                            <option value="A+">A+</option>
                                                            <option value="A-">A-</option>
                                                            <option value="B+">B+</option>
                                                            <option value="B-">B-</option>
                                                            <option value="AB+">AB+</option>
                                                            <option value="AB-">AB-</option>
                                                        </select>
                                                        @error('blood')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">বাতিল</button>
                                                    <button type="submit" class="btn btn-success">প্রোফাইল চালু করুন</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- /.card -->
                            <!-- /.card -->
                        </section>
                        <!-- right col -->
                    </div>
                    <!-- /.row (main row) -->
                </div><!-- /.container-fluid -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

@endsection

@section('css')
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('') }}/plugins/fontawesome-free/css/all.min.css">
    <link rel="stylesheet" href="{{ asset('') }}/dist/css/adminlte.min.css">
{{--    <!-- overlayScrollbars -->--}}
{{--    <link rel="stylesheet" href="{{ asset('') }}/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">--}}
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('') }}/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="{{ asset('') }}/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
{{--    <!--  Google Font: Source Sans Pro -->--}}
{{--    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">--}}
@endsection

@section('script')
    <!-- jQuery -->
    <script src="{{ asset('') }}/plugins/jquery/jquery.min.js"></script>
{{--    <!-- jQuery UI 1.11.4 -->--}}
{{--    <script src="{{ asset('') }}/plugins/jquery-ui/jquery-ui.min.js"></script>--}}
{{--    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->--}}
{{--    <script>--}}
{{--        $.widget.bridge('uibutton', $.ui.button)--}}
{{--    </script>--}}
{{--    --}}{{--    Popper JS--}}
{{--    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>--}}
    <!-- Bootstrap 4 -->
    <script src="{{ asset('') }}/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- AdminLTE App -->
    <script src="{{ asset('') }}/dist/js/adminlte.js"></script>

    <!-- DataTables -->
    <script src="{{ asset('') }}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{{ asset('') }}/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="{{ asset('') }}/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
    <script src="{{ asset('') }}/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>

    <script>
        $(function () {
            $("#example1").DataTable({
                "responsive": true,
                "autoWidth": false,
            });
        });
    </script>
    <script>
        function handleDelete($id) {
            var form = document.getElementById('deleteProfileForm')
            form.action = '/profile/' + $id
            $('#deleteModal').modal('show')
        }
    </script>
    <script>
        function handleContinue($id) {
            var form = document.getElementById('continueProfileForm')
            form.action = '/profile/continue/' + $id
            $('#continueModal').modal('show')
        }
    </script>
@endsection
