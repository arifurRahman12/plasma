<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Plasma</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #E7B124;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .m-b-md {
            margin-bottom: 30px;
            font-weight: bold;
        }

        .btn-group .btn {
            margin-left: 15px;
            margin-bottom: 25px;
            color: white;
            padding: 15px 32px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 16px;
            cursor: pointer;
            float: left;
            border-radius: 10px;
        }

        .button {
            display: inline-block;
            border-radius: 4px;
            background-color: #f4511e;
            border: none;
            color: #FFFFFF;
            text-align: center;
            font-size: 28px;
            padding: 8px;
            width: 200px;
            transition: all 0.5s;
            cursor: pointer;
            margin: 5px;
        }

        .button span {
            cursor: pointer;
            display: inline-block;
            position: relative;
            transition: 0.5s;
        }

        .button span:after {
            content: '\00bb';
            position: absolute;
            opacity: 0;
            top: 0;
            right: -20px;
            transition: 0.5s;
        }

        .button:hover span {
            padding-right: 25px;
        }

        .button:hover span:after {
            opacity: 1;
            right: 0;
        }
        P{
            font-weight: bold;
        }
        .show{
            display: flex;
            justify-content: center;
        }
    </style>
</head>
<body>
<div class="flex-center position-ref full-height">
    <div class="content">
        <div class="title m-b-md" style="color: #571845">
            Plasma
        </div>
        <div style="margin-top: 40px">
            <a class="button" style="vertical-align:middle" href="/home"><span>প্লাজমা দাতা খুঁজুন</span></a>
        </div>
        <div>
            <p class="text-center font-weight-bold">আপনি করোনায় আক্রান্ত হয়ে সুস্থ হয়ে থাকলে দয়া করে রেজিস্ট্রেশন করুন</p>
        </div>
        <div class="show">
            <div class="btn-group">
                @if (Route::has('login'))
                    @auth

                    @else
                        <a class="btn" style="background-color: #5cb85c" href="{{ route('login') }}"><span>লগইন </span></a>

                        @if (Route::has('register'))
                            <a class="btn" style="background-color: #0275d8" href="{{ route('register') }}">রেজিস্ট্রেশন</a>
                        @endif
                    @endauth
                @endif
            </div>
        </div>

    </div>
</div>
</body>
</html>
