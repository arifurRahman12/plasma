<footer class="main-footer">
    <b>Developed by</b> <a href="https://facebook.com/arifurrahmanbd">Arifur Rahman</a>
    <div class="float-right d-none d-sm-inline-block">
    </div>
</footer>

@yield('script')

</div>
</body
</html>
