<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{ route('home') }}" class="brand-link">
        <img src="{{ asset('') }}dist/img/logo.jpg" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
             style="opacity: .8">
        <span class="brand-text font-weight-light">Plasma</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                        <!-- Add icons to the links using the .nav-icon class
                             with font-awesome or any other icon font library -->
                        <li class="nav-item has-treeview">
                            <a href="#" class="nav-link active">
                                <p>
                                    <i class="fas fa-user mr-2"></i>
                                    ম্যানেজ প্রোফাইল
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="{{ route('profile.edit', Auth::user()->id) }}" class="nav-link">
                                        <i class="fas fa-edit text-primary nav-icon"></i>
                                        <p>প্রোফাইল এডিট করুন</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('profile.pause', Auth::user()->id) }}" class="nav-link">
                                        <i class="fas fa-pause text-warning nav-icon"></i>
                                        <p>লিস্ট থেকে প্রোফাইল মুছুন</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#" class="nav-link" onclick="handleContinue({{ Auth::user()->id }})">
                                        <i class="fas fa-play text-success nav-icon"></i>
                                        <p>লিস্টে আবার ফিরে আসুন</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#" class="nav-link active" onclick="handleDelete({{ Auth::user()->id }})">
                                        <i class="fas fa-trash text-danger nav-icon"></i>
                                        <p>প্রোফাইল ডিলিট করুন</p>
                                    </a>
                                </li>
                            </ul>
                    <li class="nav-item has-treeview">
                        <a class="nav-link bg-danger mt-2" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            <i class="fas fa-sign-out-alt mr-2"></i>
                            <p>লগ আউট</p>
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </li>
            </ul>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
