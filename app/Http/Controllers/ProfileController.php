<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        return view('edit')->with('user',$user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);

        $this->validate($request, [
            'name' => 'required',
            'phone' => 'required|string|min:11|max:11|unique:users,phone,'.$user->id,
        ]);

        $user->update([
            'name' => $request->name,
            'phone' => $request->phone,
            'blood' => $request->blood,
            'gotWell' => isset($request->gotWell) ? $request->gotWell : now() ,
        ]);

        return redirect(route('home'))->with('success', 'আপনার প্রোফাইল টি আপডেট হয়েছে');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);

        $user->delete();

        return redirect('/');
    }

    public function pause(Request $request, $id)
    {
        $user = User::findOrFail($id);

        $user->update([
            'blood' => null
        ]);

        return redirect(route('home'))->with('warning', 'তালিকা থেকে আপনার নাম্বার মুছা হল!  আবার তালিকায় ফিরে আসতে চাইলে ম্যানেজ প্রোফাইল থেকে আবার ফিরে আসুন বাটন চাপুন');
    }

    public function continue(Request $request, $id)
    {
        $user = User::findOrFail($id);

        $this->validate($request, [
            'blood' => 'required'
        ]);

        $user->update([
            'blood' => $request->blood,
        ]);

        return redirect(route('home'))->with('success', 'আপনার নাম্বার পুনরায় তালিকাভুক্ত হল');
    }
}
