<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AddDonorController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('createDonor');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => ['required', 'string', 'max:255'],
            'phone' => ['unique:users','required', 'max:11', 'min:11'],
            'password' => ['required', 'string', 'min:4', 'confirmed'],
        ]);

        User::create([
            'role' => $request->role,
            'name' => $request->name,
            'phone' => $request->phone,
            'password' => Hash::make($request->password),
            'blood' => isset($request['blood']) ? $request['blood'] : null,
            'gotWell' => isset($request['gotWell']) ? $request['gotWell'] : now(),
        ]);

        return redirect(route('home'))->with('success', 'আপনি নতুন একজন দাতা এড করেছেন');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $donor = User::findOrFail($id);

        return view('editDonor')->with('donor', $donor);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $donor = User::findOrFail($id);

        $this->validate($request, [
            'name' => ['required', 'string', 'max:255'],
            'phone' => ['unique:users,phone,'.$donor->id,'required', 'max:11', 'min:11'],
        ]);

        $donor->update([
            'name' => $request->name,
            'phone' => $request->phone,
            'blood' => isset($request['blood']) ? $request['blood'] : null,
            'role' => $request->role
        ]);

        return redirect(route('home'))->with('success', 'আপনি দাতার প্রোফাইল আপডেট করেছেন');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
